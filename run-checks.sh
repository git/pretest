#!/bin/sh

## First - recent versions of common OSes
## then  - less common OSes
## then  - older versions of OSes

# Skip subdirs with 'make check'
# (e.g don't check gnulib when make-checking coreutils)
# but skipping SUBDIRS skips ALL tests grep...
#make_check_params="-n SUBDIRS=."

skip_other_compilers=
skip_cross_compilers=y
skip_vms=

##Old systems, not tested any more
TOOOLD="
workdir/dilos137.build-ready.qcow2
workdir/fedora20.build-ready.qcow2
workdir/hurd05.build-ready.qcow2
workdir/openbsd55.build-ready.qcow2
workdir/opensuse132.build-ready.qcow2
workdir/freebsd10.build-ready.qcow2
"

VMS="workdir/centos7.build-ready.qcow2
workdir/openbsd58.build-ready.qcow2
workdir/debian81.build-ready.qcow2
workdir/fedora23.build-ready.qcow2
workdir/freebsd101.build-ready.qcow2
workdir/opensuse421.build-ready.qcow2
workdir/ubuntu15.build-ready.qcow2
workdir/ubuntu1504-32bit.build-ready.qcow2
workdir/netbsd70.build-ready.qcow2
workdir/alpine-3.3.3.build-ready.qcow2

workdir/centos65.build-ready.qcow2

workdir/minixR330.build-ready.qcow2
workdir/hurd07.build-ready.qcow2
workdir/debian7-kfreebsd.build-ready.qcow2

workdir/opensuse131.build-ready.qcow2
workdir/trisquel7.build-ready.qcow2
workdir/debian76.build-ready.qcow2
workdir/fedora22.build-ready.qcow2
workdir/fedora21.build-ready.qcow2
workdir/freebsd93.build-ready.qcow2
workdir/trisquel601.build-ready.qcow2
workdir/gnewsense31.build-ready.qcow2
workdir/openbsd57.build-ready.qcow2
workdir/openbsd56.build-ready.qcow2
workdir/ubuntu14.build-ready.qcow2
workdir/netbsd614.build-ready.qcow2
workdir/hurd06.build-ready.qcow2
workdir/openindiana-hipster-20150330.build-ready.qcow2
"

die()
{
    BASE=$(basename "$0")
    echo "$BASE: error: $@" >&2
    exit 1
}


# http://10.0.2.2 inside a QEMU Guest is the localhost of the HOST.
#REPORT_URL=http://10.0.2.2:5167/upload
#REPORT_URL="http://localhost:5167/upload"
REPORT_URL=http://pretest.housegordon.org/upload

ANYFAIL=0
TARBALL="$1"
test -z "$TARBALL" && die "missing tarball URL"
echo "$TARBALL" | grep -qE '^(http|ftp)://' \
    || die "tarball URL ($TARBALL) doesn't start with http or ftp"

DIR=$(mktemp -d) || die "failed ot create temp dir"
T="$DIR/log.txt"

##
## Build with several different compilers
##
if test -z "$skip_other_compilers" ; then
    echo "gcc-4.8 g++-4.8
    gcc-4.9 g++-4.9
    gcc-5.2 g++-5.2
    clang-3.5 clang++-3.5
    clang-3.8 clang++-3.8
    gcc-6.1 g++-6.1
    tcc XXX" | while read _CC _CXX ; do
        NAME="local-$_CC"
        echo "Starting local build with compiler $_CC '...(log = $DIR/$NAME.log )"
        timeout 1h \
            ./misc_scripts/pretest-auto-build-check \
                -c "CC=$_CC" \
                -c "CXX=$_CXX" \
                $make_check_params \
                -r "$REPORT_URL" \
                "$TARBALL" \
                 1> "$DIR/$NAME.log" 2>&1 \
                && RES=OK || RES=ERROR

        echo "$NAME - $RES" | tee -a "$T"
    done
fi

##
## Build With Cross-Compilers
## (see http://pretest.nongnu.org/manual/#Debian-Compilers-Pack-Usage )
if test -z "$skip_cross_compilers" ; then
    for COMP in \
        arm-linux-gnueabi \
        powerpc-linux-gnu \
        mips-linux-gnu \
        i686-w64-mingw32 \
        x86_64-w64-mingw32 ;
    do
        NAME="cross-compiler-$COMP"
        echo "Starting Cross-Compilation build with compiler $COMP '...(log = $DIR/$NAME.log )"
        timeout 1h \
            ./pretest-run.pl "workdir/debian76.comp-pack.qcow2" -- \
                pretest-auto-build-check \
                    -c "--host=$COMP" \
                    -r "$REPORT_URL" \
                    "$TARBALL" \
                     1> "$DIR/$NAME.log" 2>&1 \
                    && RES=OK || RES=ERROR

        echo "$NAME - $RES" | tee -a "$T"
    done
fi

##
## build using VMs
##
if test -z "$skip_vms" ; then
    for V in $VMS ; do
        test -e "$V" || {
            echo "Error: VM '$V' does not exist" >> "$T"
            continue
        }
        NAME=$(basename "$V" .build-ready.qcow2)

        echo "Starting build on '$NAME'...(log = $DIR/$NAME.log )"
        timeout 1h \
            ./pretest-run.pl "$V" -- \
                pretest-auto-build-check \
                    $make_check_params \
                    -r "$REPORT_URL" "$TARBALL" \
                 1> "$DIR/$NAME.log" 2>&1 \
                && RES=OK || RES=ERROR

        echo "$NAME - $RES" | tee -a "$T"
    done
fi

echo
echo
echo
cat "$T"
echo "Check logs in $DIR"
exit $ANYFAIL
