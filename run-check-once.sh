#!/bin/sh

# Skip subdirs with 'make check'
# (e.g don't check gnulib when make-checking coreutils)
make_check_params="SUBDIRS=."


die()
{
    BASE=$(basename "$0")
    echo "$BASE: error: $@" >&2
    exit 1
}


# http://10.0.2.2 inside a QEMU Guest is the localhost of the HOST.
#REPORT_URL=http://10.0.2.2:5167/upload
#REPORT_URL="http://localhost:5167/upload"
REPORT_URL=http://pretest.housegordon.org/upload

TARBALL="$1"
test -z "$TARBALL" && die "missing tarball URL"
echo "$TARBALL" | grep -qE '^(http|ftp)://' \
    || die "tarball URL ($TARBALL) doesn't start with http or ftp"

VM="$2"
test -z "$VM" && die "missing VM qcow2 file"
test -e "$VM" || die "VM qcow2 file ($VM) not found"

echo "Starting build on '$NAME'...(log = $DIR/$NAME.log )"
timeout 1h \
    ./pretest-run.pl "$VM" -- \
        pretest-auto-build-check \
            -n "$make_check_params" \
            -r "$REPORT_URL" "$TARBALL" \
