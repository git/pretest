#!/usr/bin/env python

import boto
import argparse
import boto.sqs
from boto.sqs.message import Message
import sys
import json
from pprint import pprint

aws_profile_name='pretestsqs'

def parse_command_line():
    parser = argparse.ArgumentParser(description="pretest message poster")
    parser.add_argument("-v", "--verbose",  help="be verbose", action="store_true")
    parser.add_argument('src_url', metavar='SOURCE', help='Source URL to pretest');
    args = parser.parse_args()
    return args

def post_pretest_job(sqs_conn, queue_name, src_url):
    q = sqs_conn.get_queue(queue_name)
    if q is None:
        raise ValueError("Queue URL not found for queue name '%s'" % (queue_name))

    data = { "src_url" : src_url } ;
    json_data = json.dumps(data)

    m = Message()
    m.set_body(json_data)
    q.write(m)


if __name__ == "__main__":
    args = parse_command_line()

    # Requires AWS env variables, or metadata role, or ~/.aws/credentials file
    s = boto.connect_sqs(profile_name=aws_profile_name)

    post_pretest_job(s, "pretest-hg-jobs",args.src_url)
    post_pretest_job(s, "pretest-disco-jobs",args.src_url)
