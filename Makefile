CSS=doc/pretest.css

WEBDIR=../pretest-website/pretest

all:
	@echo "nothing to do"

.PHONY: clean
clean:
	rm -rf web/

.PHONY: web
web: website

.PHONY: website
website: web/versions/index.html \
	 web/manual/index.html \
	 gen-data \
	 copy-website

.PHONY: gen-data
gen-data:
	./misc_scripts/update-website.py

.PHONY: copy-website
copy-website:
	cp ./web/versions/index.html $(WEBDIR)/versions/index.html
	cp ./web/downloads/index.html $(WEBDIR)/downloads/index.html
	cp ./web/vm-images.js $(WEBDIR)/vm-images.js

os-versions.html: doc/vm-sizes.txt
	misc_scripts/build-os-versions-table.sh

web/versions/index.html: os-versions.html
	mkdir -p web/versions/
	cp os-versions.html web/versions/index.html

web/manual/index.html: doc/pretest.texi $(CSS)
	texi2any --set-customization-variable WORDS_IN_PAGE=0 \
	         --html --no-split \
	         --css-include="$(CSS)" \
	         --output "$@" \
	         -- "$<"
