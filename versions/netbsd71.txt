autoconf-version: autoconf (GNU Autoconf) 2.69
automake-version: automake (GNU automake) 1.15
autopoint-version: /usr/pkg/bin/autopoint (GNU gettext-tools) 0.19.8.1
autoreconf-version: autoreconf (GNU Autoconf) 2.69
make-version: no-version
gmake-version: GNU Make 4.1
makeinfo-version: makeinfo (GNU texinfo) 4.8
git-version: git version 2.12.0
wget-version: GNU Wget 1.19.1 built on netbsd.
rsync-version: rsync  version 3.1.2  protocol version 31
gcc-version: gcc (nb2 20150115) 4.8.5
cc-version: cc (nb2 20150115) 4.8.5
uname-s: NetBSD
uname-r: 7.1
uname-m: amd64
uname-p: x86_64
uname-i: 
uname-o: 
uname-v: NetBSD 7.1 (GENERIC.201703111743Z)
