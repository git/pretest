#!/usr/bin/env python

import boto
import boto.sqs
import time
import sys
import argparse
import json
from subprocess import Popen,PIPE
from pprint import pprint
import codecs

codecs.getwriter('utf8')(sys.stdout)

aws_profile_name='pretestsqs'
email = "assafgordon@gmail.com"

def parse_command_line():
    parser = argparse.ArgumentParser(description="pretest message runner")
    parser.add_argument("-v", "--verbose",  help="be verbose", action="store_true")
    parser.add_argument("-m", "--mode",  help="operation mode",
                        choices=["hg","disco"], required=True)
    args = parser.parse_args()
    return args


def get_queued_src_url(queue_name):
    # Requires AWS env variables, or metadata role, or ~/.aws/credentials file
    s = boto.connect_sqs(profile_name=aws_profile_name)
    q = s.get_queue(queue_name)
    if q is None:
        raise ValueError("Queue URL not found for queue name '%s'" % (queue_name))

    # Instead of 'get_messages()' use the simpler 'read()'
    msg = q.read()
    if msg is None:
        #sys.exit("No messages available from queue '%s'" % (qname))
        return None

    try:
        data = json.loads(msg.get_body())
    except ValueError as e:
        raise ValueError("received invalid JSON data from SQS queue '%s': msgid '%s' data '%s'" % \
                        (qname, msg.id, msg.get_body()))

    src_url = data['src_url']

    if not q.delete_message(msg):
        raise IOError("failed to delete SQS message from queue '%s' msgid '%s'" % (qname, msg.id))

    return src_url

def send_notification_email(subject,to,body):
    p = Popen(["mail",
               "-s", subject,
               to], shell=False, stdin=PIPE)
    p.communicate(input=body + "\n\n\n\n\n")
    p.wait()


def run_pretest_job_on_disco(src_url):
    send_notification_email("Disco-Pretest-Started: " + src_url, email, src_url)

    p = Popen(["sh", "run-checks.sh", src_url],
               shell=False, stdout=PIPE, stderr=PIPE,
               cwd="/localdata/gordon/virtual_machine/pretest")
    (sout,serr) = p.communicate()
    p.wait()

    body = """Building: %s

== STDOUT ==

%s

== STDERR ==

%s

""" % ( src_url, sout, serr )

    # Python POpen/Communicate can't handle too-long inputs (argggg!)
    # trim to less than 4K
    body = body[:4000]

    send_notification_email("Disco-Pretest-Ended: " + src_url, email, body)
    return None

def run_pretest_job_on_hg(src_url):
    send_notification_email("HG-Pretest-Started: " + src_url, email, src_url)

    # This will return immediately
    p = Popen(["sh", "pretest-run-remote-checks.sh", src_url],
               shell=False, cwd="/home/gordon")
    p.communicate()
    p.wait()

    loops = 0
    counts = 1;
    # Wait until all the background 'screen's have terminated,
    # check them every 30 seconds, allow up to 90 minutes of testing
    while counts > 0 and loops < (90*2):
        time.sleep(30)
        cmd = "screen -list | grep '\.pretest-' | wc -l"
        sout = subprocess.check_output(cmd, shell=True).strip()
        counts = int(sout)
        if counts == 0:
            break
        loops = loops + 1

    body = src_url ;
    if counts == 0:
        body = body + " - terminated OK"
    else:
        body = body + " - not terminated after %d minutes" % (loops / 2)

    send_notification_email("HG-Pretest-Ended: " + src_url, email, body)
    return None



if __name__ == "__main__":
    args = parse_command_line()

    if args.mode == "disco":
        queue_name = "pretest-disco-jobs"
    elif args.mode == "hg":
        queue_name = "pretest-hg-jobs"

    src_url = get_queued_src_url(queue_name)
    if src_url is None:
        # no more messages
        print "no messages found."
        sys.exit(0)


    if args.mode == "disco":
        run_pretest_job_on_disco(src_url)
    elif args.mode == "hg":
        run_pretest_job_on_hg(src_url)

